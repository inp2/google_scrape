# google_scrape

A data scraping tool used for extracting names and important entities from top google search
websites. The tool is initially provided an entities and fetches the top 5 google search web
pages. Once fetched, the web page is extracted, parseed, reformatted, and relies on
basic natural language processing to determine important entities. This information
is return as a list of words.

## Prerequisites

`sys, html2text, googlesearch, nltk, nltk.tokenize, requests, urllib, nameparser.parser`

## Example

`python3 google_scrape.py <keyword>`

`python3 google_scrape.py "Donald Trump`
